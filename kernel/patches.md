# Kernel Patches

## Sources
- [Linux kernel coding style](https://www.kernel.org/doc/html/latest/process/coding-style.html)
- [Patch Series](https://kernelnewbies.org/PatchSeries)
- [Submitting your first patch to the Linux kernel](https://www.bytelab.codes/submitting-your-first-patch-to-the-linux-kernel/)
